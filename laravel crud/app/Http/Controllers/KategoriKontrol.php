<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class KategoriKontrol extends Controller
{
    public function create()
    {
        return view('kategori.tambah');
    }

    public function store(Request $request)
    {
        $request->validate([
            'id' => 'required',
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required'
        ]);
    }
}

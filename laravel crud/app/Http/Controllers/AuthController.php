<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use function GuzzleHttp\Promise\all;

class AuthController extends Controller
{
    public function register()
    {
        return view('page.register');
    }

    public function welcome(Request $request)
    {
        //buat nampung semua
        // dd($request->all());
        $first = $request['firstname'];
        $second = $request['lastname'];

        return view('page.welcome', ['first' => $first, 'second' => $second]);
    }
}

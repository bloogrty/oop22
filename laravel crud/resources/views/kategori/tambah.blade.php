
@extends('layout.master')



@section('judul')
Halaman tambah kategori
@endsection

@section('content')
    
<form action="kategori" method="POST">
  @csrf
    <div class="form-group">
      <label>id</label>
      <input type="text" name="id" class="form-control">
    </div>
    @error('id')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror

    <div class="form-group">
      <label>nama</label>
      <textarea name="nama"cols="30" rows="10" class="form-control"></textarea>
    </div>
    @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror

    <div class="form-group">
      <label>umur</label>
      <textarea name="umur"cols="30" rows="10" class="form-control"></textarea>
    </div>
    @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror


    <div class="form-group">
      <label>bio</label>
      <textarea name="bio"cols="30" rows="10" class="form-control"></textarea>
    </div>
    @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror

    <button type="submit" class="btn btn-primary">Submit</button>
  </form>


@endsection

{{-- id nama umur bio --}}
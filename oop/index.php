<?php

require_once('animal.php');
require_once('ape.php');
require_once('frog.php');

$sheep = new Animal("shaun");

echo "Nama hewan : " . $sheep->name . "<br>"; // "shaun"
echo "Jumlah kaki : " . $sheep->legs . "<br>"; // 4
echo "darah dingin : " . $sheep->cold_blooded . "<br><br>"; // "no"

// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())
$object1 = new Frog("buduk");

echo "Nama hewan : " . $object1->name . "<br>";
echo "Jumlah kaki : " . $object1->legs . "<br>";
echo "Darah dingin : " . $object1->cold_blooded . "<br>";
$object1->jump();


$object2 = new Ape("kera sakti");

echo "Nama hewan : " . $object2->name . "<br>";
echo "Jumlah kaki : " . $object2->legs . "<br>";
echo "Darah dingin : " . $object2->cold_blooded . "<br>";
$object2->yell();

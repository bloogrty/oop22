<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'utama']);

//klo gk pake controller ini syntaxnya, klo pake jadi yang kayak diatasa

// Route::get('/', function () {
//     $name = "uyaya aaaa";
//     return view('welcome');
// });

Route::get('/register', [AuthController::class, 'register']);



Route::post('/kirim', [AuthController::class, 'welcome']);

// Route::get('/biodata', function () {
//     return view('bio.biodata');
// });
